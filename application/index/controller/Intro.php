<?php
/**
 * Created by PhpStorm.
 * User: jiankian
 * Date: 2017/7/25
 * Time: 上午3:14
 */

namespace app\index\controller;



use Grafika\Color;
use Grafika\Grafika;

class Intro extends Base
{
    public function index()
    {
        return $this->fetch();
    }
    /*
     * 姓名坐标  x,y:189,323
     * 性别坐标  x,y:384.6,323
     *
     * 出生年份坐标  x,y:447,323
     * 出生月份坐标  x,y:580,323
     * 出生日号坐标  x,y:675,323
     *
     * 入学年份坐标  x,y:850,323
     * 入学月份坐标  x,y:118,370
     * 毕业年份坐标  x,y:229,370
     * 毕业月份坐标  x,y:364,370
     *
     * 专业名称坐标  x,y:528,370
     * 年制名称坐标  x,y:150,423
     * 科目名称坐标  x,y:314,423
     *
     *
     * 校名坐标 x,y:253,557
     * 校长坐标 x,y:763,557
     * 证书编号 x,y:203,621
     *
     * 发证日期
     * 年 x,y:600,621
     * 月 x,y:741,621
     * 日 x,y:840,621
     *
     * 照片坐标 x,y:784.2,81
     * 照片大小  x,y:117,150
     * y轴减少24px
     */
    public function act()
    {
        if($this->request->isPost()){
            // 获取表单上传文件 例如上传了001.jpg
            $file = request()->file('zp');

            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads'.DS.'img');
            if($info){
                // 成功上传后 获取上传信息
                // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
                $img = ROOT_PATH . 'public' . DS . 'uploads'.DS.'img'.DS.$info->getSaveName();
            }else{
                // 上传失败获取错误信息
//                echo $file->getError();
                $img = ROOT_PATH.'public/res/img/avatar.jpg';
            }
//            图片上传处理结束
            $sr = strtotime(input('sr'));
            $rx = strtotime(input('rx'));
            $lx = strtotime(input('lx'));
            if (input('font') == 'simkai'){
                $font = ROOT_PATH.'public/res/font/simkai.ttf';
            }else if (input('font') == 'pingfang'){
                $font = ROOT_PATH.'public/res/font/PingFangMedium.ttf';
            }else{
                $font = '';
            }
            $size = input('fontsize');
            $color = new  Color(input('color'));
            $editor = Grafika::createEditor();
            $editor->open($image,ROOT_PATH.'public/res/img/mb1.jpg');

//            图片上打印文字
//            姓名
            $editor->text($image,input('xm'),$size,189,299,$color,$font,0);
//            性别
            $editor->text($image,input('xb'),$size,385,299,$color,$font,0);
//            出生年
            $editor->text($image,date('Y',$sr),$size,447,299,$color,$font,0);
//            出生月
            $editor->text($image,date('m',$sr),$size,580,299,$color,$font,0);
//            出生日
            $editor->text($image,date('d',$sr),$size,675,299,$color,$font,0);
//            入校年 月  离校 年  月
            $editor->text($image,date('Y',$rx),$size,850,299,$color,$font,0);
            $editor->text($image,date('m',$rx),$size,118,346,$color,$font,0);
            $editor->text($image,date('Y',$lx),$size,229,346,$color,$font,0);
            $editor->text($image,date('d',$lx),$size,365,346,$color,$font,0);
//            专业 年制 科目
            $editor->text($image,input('zy'),$size,528,346,$color,$font,0);
            $editor->text($image,input('nz'),$size,150,400,$color,$font,0);
            $editor->text($image,input('km'),$size,314,400,$color,$font,0);

//            校名  校长
            $editor->text($image,"安浪科技学院",$size,253,532,$color,$font,0);
            $editor->text($image,"安浪哥哥",$size,763,532,$color,$font,0);
//            编号 发证 年 月 日
            $editor->text($image,"ANN".date("Ymd").rand(100,999),$size,203,597,$color,$font,0);
            $editor->text($image,date('Y'),$size,600,597,$color,$font,0);
            $editor->text($image,date('m'),$size,741,597,$color,$font,0);
            $editor->text($image,date('d'),$size,840,597,$color,$font,0);

//            添加照片--->
//            先打开一个新图片的编辑器
            $editor->open($image0,$img);
//            $editor->resizeExactHeight($image0,150);
            $editor->resizeExact($image0,115,149);
//            $editor->crop($image0,117,150,'smart');//智能裁剪成证件照

//            覆盖照片
            $editor->blend($image,$image0,'normal',1.0,'top-left',786,83);
            header('Content-type: image/png'); // Tell the browser we're sending a png image
            $image->blob('PNG');
        }else{
            $this->error('禁止非法请求',url('index'));
        }
    }
}